using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Models;
using tech_test_payment_api.Context;
using Microsoft.EntityFrameworkCore;

namespace tech_test_payment_api.Controllers
{
    
    [ApiController]
    [Route("[controller]")]
    public class RegistraVendaController : ControllerBase
    {
        RegraPagamentoEnum rp = new RegraPagamentoEnum();
        private readonly OrganizadorContext _context;

        public RegistraVendaController(OrganizadorContext context)
        {
            _context = context;
        }

        [HttpGet("{id}")]
        public IActionResult ObterVendaId(int id)
        {
            var registraVenda = _context.RegistraVendas.Find(id);

            if(registraVenda == null)
                return NotFound();

            return Ok(registraVenda);
        }

    
        [HttpPost]
        public IActionResult CriarRegistraVenda (RegistraVenda registraVenda)
        {


            if(registraVenda.venda.ItensVendido.Count == 0)
            {
                return BadRequest(new { Erro = $"Sem itens na lista" });
            }
            
         
            if(registraVenda.venda.vendedor.CpfVendedor != null)
            {
                _context.Entry(registraVenda.venda.vendedor).State = EntityState.Unchanged;
            }

            _context.Add(registraVenda);
            
            _context.SaveChanges();
            return CreatedAtAction(nameof(ObterVendaId), new{ id = registraVenda.Id}, registraVenda);
           
        }

        [HttpPut("{id}")]
        public IActionResult AtualizarRegistraVenda (int id, EnumStatusVenda status)
        {
            
            var registroVenda = _context.RegistraVendas.Find(id);
            var statusAtual = registroVenda.Status;

            RegistraVenda registroVend = new RegistraVenda();
            if (registroVenda == null)
                return NotFound();

            var registraVenda = _context.RegistraVendas.Where(x => x.Status == status);
           
            registroVenda.Status = status;
           
            
            if((rp.validarPagamento(rp.converterEnum(statusAtual), rp.converterEnum(status))) == false)
                return BadRequest(new { Erro = "Status atualizado de forma inesperada" });

            _context.RegistraVendas.Update(registroVenda);
            _context.SaveChanges();

            return Ok();           
            

        }

        [HttpGet("ObterTodosIdRegistroVenda")]
        public IActionResult ObterTodosIdRegistroVenda()
        {
            var registrovendas = _context.RegistraVendas;

            if(registrovendas == null)
                return NotFound();

            return Ok(registrovendas);
        }


    }
}
using Microsoft.EntityFrameworkCore;
using tech_test_payment_api.Context;
using System.Text.Json.Serialization;
using Microsoft.OpenApi.Models;
using Microsoft.EntityFrameworkCore.Diagnostics;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

//Configuração para Rodar Banco em memoria.
builder.Services.AddDbContext<OrganizadorContext>(opt => opt.UseInMemoryDatabase("PaymentApi"));

//Configuração para rodar com Banco de Dados Sql Express
// builder.Services.AddDbContext<OrganizadorContext>(options =>
//     options.UseSqlServer(builder.Configuration.GetConnectionString("ConexaoPadrao")));

//  builder.Services.AddDbContext<OrganizadorContext>(options =>
//  options.ConfigureWarnings(x => x.Ignore(InMemoryEventId.TransactionIgnoredWarning)
//  ));


builder.Services.AddControllers().AddJsonOptions(options =>
    options.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter()));

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(options =>
{
    options.SwaggerDoc("v1", new OpenApiInfo
    {
        Version = "v1",
        Title = "tech-test-payment-api",
        Description = "API Pottencial Bootcamp na DIO"
    });
});


var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "tech-test-payment-api"));
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();

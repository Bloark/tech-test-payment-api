using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Context
{
    public class OrganizadorContext : DbContext
    {
        public OrganizadorContext(DbContextOptions<OrganizadorContext> options) : base(options)
        {
            
        }

        public DbSet<RegistraVenda> RegistraVendas { get; set; }
        public DbSet<Item> Itens { get; set; }       
        public DbSet<Vendedor> Vendedores { get; set; }
        public DbSet<Venda> venda { get; set; }        


           protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<RegistraVenda>();
            modelBuilder.Entity<Item>();
            modelBuilder.Entity<Vendedor>();
            modelBuilder.Entity<Venda>();

        }

        

    }
    
}
    

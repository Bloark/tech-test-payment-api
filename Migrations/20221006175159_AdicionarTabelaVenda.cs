﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace tech_test_payment_api.Migrations
{
    public partial class AdicionarTabelaVenda : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Vendedores",
                columns: table => new
                {
                    CpfVendedor = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    NomeVendedor = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    EmailVendedor = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TelefoneVendedor = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Vendedores", x => x.CpfVendedor);
                });

            migrationBuilder.CreateTable(
                name: "venda",
                columns: table => new
                {
                    IdentificadorPedido = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    vendedorCpfVendedor = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    dataVenda = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_venda", x => x.IdentificadorPedido);
                    table.ForeignKey(
                        name: "FK_venda_Vendedores_vendedorCpfVendedor",
                        column: x => x.vendedorCpfVendedor,
                        principalTable: "Vendedores",
                        principalColumn: "CpfVendedor");
                });

            migrationBuilder.CreateTable(
                name: "Itens",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    NomeProduto = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PreçoProduto = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    VendaIdentificadorPedido = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Itens", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Itens_venda_VendaIdentificadorPedido",
                        column: x => x.VendaIdentificadorPedido,
                        principalTable: "venda",
                        principalColumn: "IdentificadorPedido");
                });

            migrationBuilder.CreateTable(
                name: "RegistraVendas",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Status = table.Column<int>(type: "int", nullable: false),
                    vendaIdentificadorPedido = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RegistraVendas", x => x.Id);
                    table.ForeignKey(
                        name: "FK_RegistraVendas_venda_vendaIdentificadorPedido",
                        column: x => x.vendaIdentificadorPedido,
                        principalTable: "venda",
                        principalColumn: "IdentificadorPedido");
                });

            migrationBuilder.CreateIndex(
                name: "IX_Itens_VendaIdentificadorPedido",
                table: "Itens",
                column: "VendaIdentificadorPedido");

            migrationBuilder.CreateIndex(
                name: "IX_RegistraVendas_vendaIdentificadorPedido",
                table: "RegistraVendas",
                column: "vendaIdentificadorPedido");

            migrationBuilder.CreateIndex(
                name: "IX_venda_vendedorCpfVendedor",
                table: "venda",
                column: "vendedorCpfVendedor");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Itens");

            migrationBuilder.DropTable(
                name: "RegistraVendas");

            migrationBuilder.DropTable(
                name: "venda");

            migrationBuilder.DropTable(
                name: "Vendedores");
        }
    }
}

﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using tech_test_payment_api.Context;

#nullable disable

namespace tech_test_payment_api.Migrations
{
    [DbContext(typeof(OrganizadorContext))]
    partial class OrganizadorContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "6.0.9")
                .HasAnnotation("Relational:MaxIdentifierLength", 128);

            SqlServerModelBuilderExtensions.UseIdentityColumns(modelBuilder, 1L, 1);

            modelBuilder.Entity("tech_test_payment_api.Models.Item", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("Id"), 1L, 1);

                    b.Property<string>("NomeProduto")
                        .HasColumnType("nvarchar(max)");

                    b.Property<decimal>("PreçoProduto")
                        .HasColumnType("decimal(18,2)");

                    b.Property<int?>("VendaIdentificadorPedido")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.HasIndex("VendaIdentificadorPedido");

                    b.ToTable("Itens");
                });

            modelBuilder.Entity("tech_test_payment_api.Models.RegistraVenda", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("Id"), 1L, 1);

                    b.Property<int>("Status")
                        .HasColumnType("int");

                    b.Property<int?>("vendaIdentificadorPedido")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.HasIndex("vendaIdentificadorPedido");

                    b.ToTable("RegistraVendas");
                });

            modelBuilder.Entity("tech_test_payment_api.Models.Venda", b =>
                {
                    b.Property<int>("IdentificadorPedido")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("IdentificadorPedido"), 1L, 1);

                    b.Property<DateTime>("dataVenda")
                        .HasColumnType("datetime2");

                    b.Property<string>("vendedorCpfVendedor")
                        .HasColumnType("nvarchar(450)");

                    b.HasKey("IdentificadorPedido");

                    b.HasIndex("vendedorCpfVendedor");

                    b.ToTable("venda");
                });

            modelBuilder.Entity("tech_test_payment_api.Models.Vendedor", b =>
                {
                    b.Property<string>("CpfVendedor")
                        .HasColumnType("nvarchar(450)");

                    b.Property<string>("EmailVendedor")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("NomeVendedor")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("TelefoneVendedor")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("CpfVendedor");

                    b.ToTable("Vendedores");
                });

            modelBuilder.Entity("tech_test_payment_api.Models.Item", b =>
                {
                    b.HasOne("tech_test_payment_api.Models.Venda", null)
                        .WithMany("ItensVendido")
                        .HasForeignKey("VendaIdentificadorPedido");
                });

            modelBuilder.Entity("tech_test_payment_api.Models.RegistraVenda", b =>
                {
                    b.HasOne("tech_test_payment_api.Models.Venda", "venda")
                        .WithMany()
                        .HasForeignKey("vendaIdentificadorPedido");

                    b.Navigation("venda");
                });

            modelBuilder.Entity("tech_test_payment_api.Models.Venda", b =>
                {
                    b.HasOne("tech_test_payment_api.Models.Vendedor", "vendedor")
                        .WithMany()
                        .HasForeignKey("vendedorCpfVendedor");

                    b.Navigation("vendedor");
                });

            modelBuilder.Entity("tech_test_payment_api.Models.Venda", b =>
                {
                    b.Navigation("ItensVendido");
                });
#pragma warning restore 612, 618
        }
    }
}

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
namespace tech_test_payment_api.Models
{
    public class Venda
    {
        [Key]
        public int IdentificadorPedido { get; set; }                 
        public Vendedor vendedor { get; set; }
        public DateTime dataVenda { get; set; }       
        public List<Item> ItensVendido { get; set; }
    }
}
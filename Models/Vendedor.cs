using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
namespace tech_test_payment_api.Models
{
    public class Vendedor
    {
        
        [Key]
        public string CpfVendedor { get; set; }
        public string NomeVendedor { get; set; }
        public string EmailVendedor { get; set; }
        public string TelefoneVendedor { get; set; }   
                
    }
}
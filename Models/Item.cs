using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace tech_test_payment_api.Models
{
    public class Item
    {   
        [Key]
        public int Id { get; set; }
        public string NomeProduto { get; set; }
        public decimal PreçoProduto  { get; set; }
    }
}
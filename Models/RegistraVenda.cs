using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace tech_test_payment_api.Models
{
    public class RegistraVenda
    {
      [Key]       
      public int Id { get; set; }
      public EnumStatusVenda Status { get; set; }
      public Venda venda { get;  set; }

    }
}
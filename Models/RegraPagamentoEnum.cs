using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel;

namespace tech_test_payment_api.Models
{
    public class RegraPagamentoEnum
    {
        public string converterEnum(Enum enumStatus)
        {
            var enumResult = (TypeDescriptor.GetConverter(enumStatus).ConvertTo(enumStatus, typeof(string))).ToString();
            return enumResult;
        }

        public Boolean validarPagamento(string statusAtual, string status)
        {
            if (statusAtual == "AguardandoPagamento" && status == "PagamentoAprovado")
            {
                return true;
            }
            if (statusAtual == "AguardandoPagamento" && status == "Cancelada")
            {
                return true;
            }
            if (statusAtual == "PagamentoAprovado" && status == "EnviadoParaTransportadora")
            {
                return true;
            }
            if (statusAtual == "PagamentoAprovado" && status == "Cancelada")
            {
                return true;
            }
            if (statusAtual == "EnviadoParaTransportadora" && status == "Entregue")
            {
                return true;
            }

            return false;
        }
    }
}